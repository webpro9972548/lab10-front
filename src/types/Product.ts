import type { Type } from "./Type"

type Product = {
  id?: number
  name: string
  price: number
  type: Type
  image: string
}
function getImageUrl(product: Product) {
  return `http://localhost:3000/Images/products/${product.id}`
}
export { type Product, getImageUrl }
